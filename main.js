const SHA256 = require('crypto-js/sha256');

class Block {
  constructor(index, timestamp, data, previousHash = '') {
    this.index = index;
    this.timestamp = timestamp;
    this.data = data;
    this.previousHash = previousHash;

    // Hash as empty
    this.hash = '';

    // Iteration flag
    this.nonce = 0;
  }

  calculateHash() {
    return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
  }

  mineBlock(difficulty) {
    while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
      // Increment flag
      this.nonce++;

      // Recalculating hash value
      this.hash = this.calculateHash();
    }
  }
}

class BlockChain {
  constructor() {
    this.chain = [this.createGenesisBlock()];
    this.difficulty = 6;
  }

  createGenesisBlock() {
    return new Block(0, '03/03/2020', 'Genesis Block', '0');
  }

  getLatestBlock() {
    return this.chain[this.chain.length-1];
  }

  addBlock(newBlock) {
    newBlock.previousHash = this.getLatestBlock().hash;
    // newBlock.hash = newBlock.calculateHash();
    newBlock.mineBlock(this.difficulty);
    this.chain.push(newBlock);

    // Console log new block's hash
    console.log(newBlock.hash);
  }

  isChainValid() {
    for(let i=1; i < this.chain.length; i++) {
      const currentBlock = this.chain[i];
      const previousBlock = this.chain[i-1];

      if (currentBlock.hash !== currentBlock.calculateHash()) {
        return false;
      } else if (currentBlock.previousHash !== previousBlock.hash) {
        return false;
      }
    }

    return true;
  }
}

// New blockchain
const koreCoin = new BlockChain();

// Adding blocks to blockchain
console.log('Mining block 1');
koreCoin.addBlock(new Block(1, '03/03/2020', {amount: 20}));
console.log('Mining block 2');
koreCoin.addBlock(new Block(2, '04/03/2020', {amount: 40}));
console.log('Mining block 3');
koreCoin.addBlock(new Block(3, '04/03/2020', {amount: 50}));

// Log data koreCoin returns
console.log(JSON.stringify(koreCoin, null, 4));

// Check validity
console.log('Is Blockchain valid?', koreCoin.isChainValid());

// Tampering with blockchain by changing one of the earlier values
koreCoin.chain[1].data = {amount: 100};
// Recalculating hash value
koreCoin.chain[1].data = koreCoin.chain[1].calculateHash();

// Checking validity after tampering
console.log('Is Blockchain valid?', koreCoin.isChainValid());
